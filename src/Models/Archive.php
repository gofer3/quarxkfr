<?php

namespace Yab\Quarx\Models;

use App\Scopes\ViewSubAgentScope;

class Archive extends QuarxModel
{
    public $table = 'archives';

    public $primaryKey = 'id';

    public $fillable = [
        'token',
        'site_id',
        'entity_id',
        'entity_type',
        'entity_data',
        'site_id'
    ];

    public static $rules = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ViewSubAgentScope );
    }
}
