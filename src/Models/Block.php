<?php

namespace Yab\Quarx\Models;

use App\Scopes\ViewSubAgentScope;

class Block extends QuarxModel
{

    public $table = 'blocks';

    public $primaryKey = 'id';

    public $guarded = ['id'];

    public static $rules = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ViewSubAgentScope);
    }

    public function __construct(array $attributes = [])
    {

    }

    public function template()
    {
        return $this->belongsToMany(Template::class, 'template_blocks', 'block_id', 'template_id');
    }

    public function getIncludeOptions()
    {

        //Using category ID right now to determine type of include
        //1 -- call to action
        switch ($this->category_id) {
            case 1:
            default:
                return [
                    [
                        'filename'     => 'call_to_action.contact_us',
                        'display_name' => 'Contact Us - Basic'
                    ],
                    [
                        'filename'     => 'call_to_action.consider_buying',
                        'display_name' => 'Contact Us - Buying',
                    ],
                    [
                        'filename'     => 'call_to_action.consider_selling',
                        'display_name' => 'Contact Us - Selling',
                    ],

                ];
        }

    }
}
