<?php

namespace Yab\Quarx\Models;

class Template extends QuarxModel
{
    public $table = 'templates';

    public $primaryKey = 'id';

    public $guarded = ['id'];

    public static $rules = [];

    public function blocks() {
        return $this->belongsToMany( Block::class, 'template_blocks', 'template_id', 'block_id' );
    }

    public function __construct(array $attributes = [])
    {

    }
}
