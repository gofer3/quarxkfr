<?php

namespace Yab\Quarx\Models;

use App\Scopes\ViewSubAgentScope;
use Yab\Quarx\Traits\Translatable;

class FAQ extends QuarxModel
{
    use Translatable;

    public $table = 'faqs';

    public $primaryKey = 'id';

    protected $guarded = [];

    public static $rules = [
        'question' => 'required',
    ];

    protected $appends = [
        'translations',
    ];

    protected $fillable = [
        'question',
        'answer',
        'is_published',
        'published_at',
        'site_id',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ViewSubAgentScope );
    }

    public function __construct(array $attributes = [])
    {
        $keys = array_keys(request()->except('_method', '_token'));
        $this->fillable(array_values(array_unique(array_merge($this->fillable, $keys))));
        parent::__construct($attributes);
    }
}
