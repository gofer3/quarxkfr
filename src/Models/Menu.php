<?php

namespace Yab\Quarx\Models;

use App\Scopes\ViewSubAgentScope;

class Menu extends QuarxModel
{
    public $table = 'menus';

    public $primaryKey = 'id';

    protected $guarded = [];

    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
    ];

    protected $fillable = [
        'name',
        'slug',
        'order',
        'site_id',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ViewSubAgentScope );
    }

    public function __construct(array $attributes = [])
    {
        $keys = array_keys(request()->except('_method', '_token'));
        $this->fillable(array_values(array_unique(array_merge($this->fillable, $keys))));
        parent::__construct($attributes);
    }
}
