<?php

namespace Yab\Quarx\Models;

use App\Scopes\ViewSubAgentScope;
use Yab\Quarx\Services\Normalizer;
use Yab\Quarx\Traits\Translatable;

class Blog extends QuarxModel
{
    use Translatable;

    public $table = 'blogs';

    public $primaryKey = 'id';

    protected $guarded = [];

    public static $rules = [
        'title' => 'required|string',
        'url' => 'required|string',
    ];

    protected $appends = [
        'translations',
        'html_tags',
    ];

    protected $fillable = [
        'title',
        'entry',
        'tags',
        'is_published',
        'seo_description',
        'seo_keywords',
        'url',
        'template',
        'published_at',
        'site_id',
        'blocks',
    ];

    protected $with = ['pageTemplate'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ViewSubAgentScope );
    }

    public function __construct(array $attributes = [])
    {
        $keys = array_keys(request()->except('_method', '_token'));
        $this->fillable(array_values(array_unique(array_merge($this->fillable, $keys))));

        parent::__construct($attributes);
    }

    public function getEntryAttribute($value)
    {
        return new Normalizer($value);
    }

    public function history()
    {
        return Archive::where('entity_type', get_class($this))->where('entity_id', $this->id)->get();
    }

    public function pageTemplate()
    {
        return $this->belongsTo(Template::class, 'template_id', 'id' );
    }

    public function getBlocksAttribute($value)
    {
        $blocks = json_decode($value, true);

        if (is_null($blocks)) {
            $blocks = [];
        }

        return $blocks;
    }

    public function getHtmlTagsAttribute() {

        $tags = $this->tags;
        $tag_list = [];

        if( $tags ) {
            $tags = explode( ",", $tags );
            foreach( $tags as $tag ) {
                $tag_list[] = ['tag' => $tag, 'link' => '/blog/tags/' . str_slug( $tag )];
            }
        }

        return $tag_list;
    }

    public function postTags() {
        return $this->belongsToMany( BlogTag::class, 'cms_blogs_tags', 'post_id', 'tag_id' );
    }



}
