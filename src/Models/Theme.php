<?php

namespace Yab\Quarx\Models;

class Theme extends QuarxModel
{
    public $table = 'themes';

    public $primaryKey = 'id';

    public $guarded = ['id'];

    public static $rules = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = 'themes';
    }

    public function getDirnameAttribute() {
        return strtolower( $this->theme );
    }
}
