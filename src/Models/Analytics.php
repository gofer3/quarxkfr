<?php

namespace Yab\Quarx\Models;

use App\Scopes\ViewSubAgentScope;

class Analytics extends QuarxModel
{
    public $table = 'analytics';

    public $primaryKey = 'id';

    public $fillable = [
        'token',
        'site_id',
        'data',
        'site_id',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ViewSubAgentScope );
    }

    public static $rules = [];
}
