<?php

namespace Yab\Quarx\Models;

use App\Scopes\ViewSubAgentScope;
use Yab\Quarx\Services\Normalizer;
use Yab\Quarx\Traits\Translatable;

class BlogTag extends QuarxModel
{
    use Translatable;

    public $table = 'blog_tag';


    protected $guarded = [
    ];

    public static $rules = [
        'name' => 'required|string',
    ];

    protected $appends = [

    ];

    protected $fillable = [
        'site_id',
        'name',
    ];

    protected $with = [];

    protected static function boot()
    {
        parent::boot();

        static::saving(function( $tag ) {

            $tag->slug = str_slug( $tag->name );

        });


//        static::addGlobalScope(new ViewSubAgentScope );
    }

    public function __construct(array $attributes = [])
    {
        $keys = array_keys(request()->except('_method', '_token'));
        $this->fillable(array_values(array_unique(array_merge($this->fillable, $keys))));

        parent::__construct($attributes);
    }

    public function posts() {
        return $this->belongsToMany( Blog::class, 'cms_blogs_tags', 'tag_id', 'post_id' );
    }


}
